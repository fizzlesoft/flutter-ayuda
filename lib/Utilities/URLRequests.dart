
class URLRequest {
  static const baseApi = 'http://pegasus-webapi.azurewebsites.net/api';
  static const URL_LOGIN = '$baseApi/User/login';
  static const URL_REGISTER = '$baseApi/User/register';
  static const URL_GET_USER_CREDENTIAL = '$baseApi/User/credential/get';
}