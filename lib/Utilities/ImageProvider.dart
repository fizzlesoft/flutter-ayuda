String getImage(ImageList image) {
  String stringImage = '';
  switch (image) {
    case ImageList.LoginBackground:
      stringImage = 'assets/images/login/background/Background@3x.png';
      break;
    case ImageList.LoginIconLogoForm:
      stringImage = 'assets/images/login/icon-logo-form/icon-logo-form@3x.png';
      break;
    case ImageList.RegisterBackground:
      stringImage = 'assets/images/register/background/bg-register@3x.png';
      break;
    case ImageList.RegisterIcon:
      stringImage = 'assets/images/register/register-icon/register-icon.png';
      break;
    case ImageList.PipeLine:
      stringImage = 'assets/images/dashboard/pipe-color/pipe-color@3x.png';
      break;
    case ImageList.DrawerBackground:
      stringImage = 'assets/images/drawer/bg-drawer/bg-drawer@3x.png';
      break;
    default:
      break;
  }
  return stringImage;
}

enum ImageList { LoginBackground, LoginIconLogoForm, RegisterBackground, RegisterIcon, PipeLine, DrawerBackground, }
