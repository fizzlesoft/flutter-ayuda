import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:firebase_auth/firebase_auth.dart';


class NetworkUtil {
  static NetworkUtil _instance = new NetworkUtil.internal();
  NetworkUtil.internal();
  factory NetworkUtil() => _instance;

  final JsonDecoder _decoder = new JsonDecoder();

  Future<dynamic> get(String url) {
    return http.get(url).then((http.Response response) {
      final String res = response.body;
      final int statusCode = response.statusCode;

      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception('Error while fetching data.');
      }

      return _decoder.convert(res);
    });
  }

  Future<dynamic> post(String url, {Map body, headers, encoding}) async {
    return await http
        .post(url, body: body, headers: headers, encoding: encoding)
        .then((http.Response response) {
      final String res = response.body;
      final int statusCode = response.statusCode;

      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception('Error while fetching data.');
      }

      return _decoder.convert(res);
    });
  }
}

class FBApi {
  static FirebaseAuth _auth = FirebaseAuth.instance;

  Future<FirebaseUser> signInEmail(String email, String password) async {
    final FirebaseUser user = await _auth.signInWithEmailAndPassword(email: email, password: password);
    final FirebaseUser currentUser = await _auth.currentUser();
    if(await user.getIdToken() != null) {
      if(currentUser.uid == user.uid)
        return user;      
    }
    return null;
  }

  Future<FirebaseUser> signUpEmail(String email, String password) async {
    final FirebaseUser user = await _auth.createUserWithEmailAndPassword(email: email, password: password);
    if(user.uid != null){
      return user;
    }
    return null;
  }
}
