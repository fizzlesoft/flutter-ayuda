import 'dart:ui' as ui;
import 'package:ayuda/Utilities/BlocProvider.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:ayuda/Utilities/ColorProvider.dart';
import 'package:location/location.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';

final size = MediaQueryData.fromWindow(ui.window).size;
Location _location = new Location();
bool _permission = false;

class AyudaMapView extends StatefulWidget implements BlocBase {
  State createState() => AyudaMapState();

  @override
  void dispose() {}
}

class AyudaMapState extends State<AyudaMapView> {
  GoogleMapController mapController;


  void initState() { 
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: new Stack(
        fit: StackFit.loose,
        children: <Widget>[
          new GoogleMap(
            onMapCreated: _onMapCreated,
          ),
          new Align(
            alignment: Alignment.bottomRight,
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                new Padding(
                  padding: EdgeInsets.only(bottom: 5.0, right: 5.0),
                  child: new SizedBox(
                    height: 56.0,
                    width: 56.0,
                    child: btnMapSettings(),
                  ),
                ),
                new Padding(
                  padding: EdgeInsets.only(bottom: 5.0, right: 5.0),
                  child: new SizedBox(
                    height: 56.0,
                    width: 56.0,
                    child: btnMyLocation(),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  FloatingActionButton btnMapSettings() {
    return new FloatingActionButton(
      backgroundColor: getColor(ColorList.WhiteCream, 1.0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
      ),
      child: Icon(
        Icons.settings,
        color: getColor(ColorList.DarkGreen, 1.0),
        size: 32,
      ),
      onPressed: () {
        print("Map Settings clicked!");
      },
    );
  }

  FloatingActionButton btnMyLocation() {
    return new FloatingActionButton(
      backgroundColor: getColor(ColorList.WhiteCream, 1.0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(10.0),
        ),
      ),
      child: Icon(
        Icons.my_location,
        color: getColor(ColorList.DarkGreen, 1.0),
        size: 32,
      ),
      onPressed: mapController == null ? null : getMyCurrentLocation,
    );
  }

  initLocationState() async {
    Position _position;
    try {
      _permission = await _location.hasPermission();
      if (_permission) {
        _position = await Geolocator()
            .getCurrentPosition(desiredAccuracy: LocationAccuracy.best);
      }
    } on PlatformException catch (e) {
      if (e.code == "PERMISSION_DENIED") {
        print("Permission denied");
      } else if (e.code == "PERMISSION_DENIED_NEVER_ASK") {
        print(
            "Permission denied - please ask the user to enable it from the app settings");
      }
    }
    
    mapController.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: LatLng(_position.latitude, _position.longitude),
          zoom: 20.0,
          tilt: 10.0,
        ),
      ),
    );
  }

  void getMyCurrentLocation() {
    initLocationState();
  }

  void _onMapCreated(GoogleMapController controller) {
    setState(() {
      mapController = controller;
    });
  }
}
