import 'package:flutter/material.dart';
class CircularRevealPainter extends CustomPainter{
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()
    ..style = PaintingStyle.fill
    ..color = Colors.blue;
    canvas.drawCircle(Offset(size.width/2, size.height/2), 24.0, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }

}