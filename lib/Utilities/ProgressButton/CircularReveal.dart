import 'package:flutter/material.dart';
import 'package:ayuda/Utilities/ProgressButton/CircularRevealPainter.dart';

class CirvularReveal extends StatefulWidget {
  _CirvularRevealState createState() => new _CirvularRevealState();
}

class _CirvularRevealState extends State<CirvularReveal> {
  @override
  Widget build(BuildContext context) {
    return new Center(
      child: CustomPaint(
        painter: CircularRevealPainter(),
      ),
    );
  }
}
