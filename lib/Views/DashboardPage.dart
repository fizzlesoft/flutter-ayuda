import 'package:ayuda/Utilities/ColorProvider.dart';
import 'package:ayuda/Utilities/ImageProvider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ayuda/Utilities/GoogleMaps/GoogleMapView.dart';
import 'dart:ui' as ui;

final size = MediaQueryData.fromWindow(ui.window).size;

final searchController = TextEditingController();
final GlobalKey<ScaffoldState> _dashboardScaffoldState =
    new GlobalKey<ScaffoldState>();
final double fontSize = 18.0;
final double iconSize = 36.0;

class DashboardPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    return new Scaffold(
      key: _dashboardScaffoldState,
      drawer: menuList(),
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60.0),
        child: AppBar(
          iconTheme: IconThemeData(
            color: getColor(ColorList.WhiteCream, 1.0),
          ),
          centerTitle: true,
          title: searchTextField(),
          backgroundColor: getColor(ColorList.DarkGreen, 1.0),
          titleSpacing: 20.0,
        ),
      ),
      body: new Container(
       child: AyudaMapView(),
      ),
      bottomNavigationBar: new Container(
        height: 100.0,
        width: MediaQuery.of(context).size.width,
        color: getColor(ColorList.DarkBlue, 1.0),
        child: Center(
          child: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: bottomView(),
          ),
        ),
      ),
    );
  }

  Container bottomView() {
    return new Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          new SizedBox(
            width: size.width,
            height: 5.0,
            child: Image.asset(
              getImage(ImageList.PipeLine),
              fit: BoxFit.fitWidth,
            ),
          ),
          SizedBox(
            height: 15.0,
          ),
          new Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              showAllGridIconButton(),
              new Padding(
                padding: EdgeInsets.only(left: 5.0),
              ),
              new Padding(
                padding: EdgeInsets.only(left: 0.0),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    new FittedBox(
                      fit: BoxFit.scaleDown,
                      child: new Text(
                        'Nearby',
                        style: new TextStyle(
                          color: getColor(ColorList.WhiteCream, 1.0),
                          fontSize: 20.0,
                        ),
                      ),
                    ),
                    new FittedBox(
                      fit: BoxFit.scaleDown,
                      child: new Container(
                        width: 150.0,
                        child: showAllContractorsAroundUser(),
                      ),
                    )
                  ],
                ),
              ),
              new Padding(
                padding: EdgeInsets.only(left: 5.0),
              ),
              new Padding(
                padding: EdgeInsets.only(right: 5.0, left: 5.0),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    showNumberOfContractorAroundUser(),
                    new Text(
                      'Results',
                      style: new TextStyle(
                        color: getColor(ColorList.WhiteCream, 1.0),
                        fontSize: 15.0,
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ],
      ),
    );
  }

  IconButton showAllGridIconButton() {
    return new IconButton(
      icon: Icon(
        Icons.keyboard_arrow_up,
      ),
      iconSize: 40.0,
      onPressed: showAllGrid,
      tooltip: 'Show all contractor around you',
      color: getColor(ColorList.WhiteCream, 1.0),
      splashColor: getColor(ColorList.CreamGreen, 1.0),
    );
  }

  Text showAllContractorsAroundUser() {
    return new Text(
      //TODO: Get through API
      'Computer Technician, Plumber, Electrician',
      overflow: TextOverflow.ellipsis,
      style: new TextStyle(color: getColor(ColorList.BaseGray, 1.0)),
    );
  }

  Text showNumberOfContractorAroundUser() {
    return new Text(
      // TODO: Must get data from the API
      '18',
      overflow: TextOverflow.ellipsis,
      maxLines: 5,
      style: TextStyle(
        color: getColor(ColorList.WhiteCream, 1.0),
        fontSize: 25.0,
      ),
    );
  }

  TextFormField searchTextField() {
    return new TextFormField(
      controller: searchController,
      style: new TextStyle(
        color: getColor(ColorList.DarkGreen, 1.0),
      ),
      autocorrect: false,
      decoration: new InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(25.0),
          borderSide: BorderSide(
              width: 1.0, color: getColor(ColorList.WhiteCream, 1.0)),
        ),
        hintText: 'Need a contractor?',
        contentPadding: EdgeInsets.only(left: 25.0),
        suffixIcon: new IconButton(
          icon: Icon(
            Icons.search,
            color: getColor(ColorList.DarkGreen, 1.0),
          ),
          onPressed: performSearch,
        ),
        hintStyle: TextStyle(
          color: getColor(ColorList.DarkGreen, 1.0),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(25.0),
          borderSide:
              BorderSide(width: 1.0, color: getColor(ColorList.BaseGray, 1.0)),
        ),
        filled: true,
        fillColor: Color.fromRGBO(254, 252, 215, 1.0),
      ),
    );
  }

  Drawer menuList() {
    return new Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          new DrawerHeader(
            child: new Text('Drawer Header'),
            decoration: BoxDecoration(
              image: new DecorationImage(
                image: AssetImage(
                  getImage(ImageList.DrawerBackground),
                ),
                fit: BoxFit.fill,
              ),
            ),
          ),
          tileText(Icons.account_box, 'Profile', ColorList.DarkGreen, viewProfile),
          tileText(Icons.info, 'Promo', ColorList.DarkGreen, viewPromo),
          tileText(Icons.history, 'History', ColorList.DarkGreen, viewHistory),
          horizontalDivider(),
          tileText(Icons.contacts, 'Contacts', ColorList.DarkGreen, viewContacts),
          tileText(Icons.settings, 'Settings', ColorList.DarkGreen, viewSettings),
          tileText(Icons.info_outline, 'About us', ColorList.DarkGreen, viewAboutUs),
          horizontalDivider(),
          tileText(Icons.person_pin, 'Be a contractor', ColorList.DarkGreen, beAContractor),
          horizontalDivider(),
          tileText(Icons.exit_to_app, 'Logout', ColorList.DarkGreen, logout)
        ],
      ),
    );
  }

  ListTile tileText(IconData icon, String text, ColorList colorList, VoidCallback callback) {
    return new ListTile(
      title: new Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          new Icon(
            icon,
            color: getColor(colorList, 1.0),
            size: iconSize,
          ),
          SizedBox(
            width: 10.0,
          ),
          new Text(
            text,
            style: TextStyle(
              color: getColor(colorList, 1.0),
              fontSize: fontSize,
            ),
          ),
        ],
      ),
      onTap: callback,
    );
  }

  Padding horizontalDivider() {
    return new Padding(
      padding: EdgeInsets.all(10.0),
      child: new Container(
        height: 1.0,
        width: size.width,
        color: getColor(ColorList.DarkGreen, 1.0),
      ),
    );
  }

  //TODO: View Profile page
  void viewProfile() {
    print('Profile is clicked');
  }

  //TODO: View Promo page
  void viewPromo() {
    print('Promo is clicked');
  }

  //TODO: View History page
  void viewHistory() {
    print('History is clicked');
  }

  //TODO: View Contact page
  void viewContacts() {
    print('Contact is clicked');
  }

  //TODO: View Settings page
  void viewSettings() {
    print('Settings is clicked');
  }

  //TODO: View About us page
  void viewAboutUs() {
    print('About us is clicked');
  }

  //TODO: View Be a Contractor page
  void beAContractor() {
    print('Be a contractor is clicked');
  }
  
  //TODO: Logout function
  void logout() {
    print('Logout is clicked');
  }

  // TODO: Show all contractor with picture.
  void showAllGrid() {
    print('Show All!');
  }

  // TODO: Show all list of Menu for regular user.
  void openMenuList() {}

  // TODO: Search on contractor in his area.
  void performSearch() {
    print(searchController.text);
  }
}
