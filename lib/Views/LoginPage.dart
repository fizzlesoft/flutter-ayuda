import 'dart:async';

import 'package:ayuda/Utilities/BlocProvider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../Utilities/ImageProvider.dart';
import '../Utilities/ColorProvider.dart';
import '../Blocs/UserLoginBloc.dart';
import 'package:connectivity/connectivity.dart';

final usernameController = TextEditingController();
final passwordController = TextEditingController();

class LoginPage extends StatefulWidget implements BlocBase {
  @override
  _LoginPage createState() => _LoginPage();

  @override
  void dispose() {
    usernameController.dispose();
    passwordController.dispose();
  }
}

class _LoginPage extends State<LoginPage> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final double textFieldHeight = 60.0;
  final double topPadding = 125.0;
  final Connectivity _connectivity = new Connectivity();

  UserLoginBloc _login = new UserLoginBloc();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;
  String _connectionStatus = 'Unknown';

  String get username => usernameController.text;
  String get password => passwordController.text;

  @override
  void dispose() {
    usernameController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    initConnectivity();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      setState(() {
        _connectionStatus = result.toString();
        if (result == ConnectivityResult.none) {
          _forceEnableConnectivity();
        }
      });
    });
  }

  Future<Null> _forceEnableConnectivity() async {
    return showDialog<Null>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new AlertDialog(
            title: new Text('Wifi/Mobile connection is disabled'),
            content: new SingleChildScrollView(
              child: new ListBody(
                children: <Widget>[
                  new Text(
                      'To continue using Ayuda app, please enable Wifi/Mobile connection.')
                ],
              ),
            ),
            actions: <Widget>[
              new FlatButton(
                child: new Text('Okay'),
                onPressed: () => Navigator.pop(context),
              )
            ],
          );
        });
  }

  void trySubmit() {
    _login.doLogin(username, password).then((bool isSuccess) {
      if (isSuccess) {
        Navigator.pushReplacementNamed(context, '/dashboard');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        decoration: new BoxDecoration(
            image: new DecorationImage(
                image: new AssetImage(getImage(ImageList.LoginBackground)),
                fit: BoxFit.cover)),
        child: new Stack(
          children: <Widget>[
            new Container(
              margin: EdgeInsets.fromLTRB(15.0, topPadding - 50, 15.0, 15.0),
              child: new Container(
                decoration: new BoxDecoration(
                    color: Color.fromRGBO(106, 131, 71, 0.5),
                    borderRadius: new BorderRadius.circular(25.0)),
                child: new Container(
                  height: 400.0,
                  padding: EdgeInsets.only(top: 60.0),
                  child: loginForm(),
                ),
              ),
            ),
            new Container(
              alignment: Alignment.topCenter,
              height: 175.0,
              margin:
                  EdgeInsets.fromLTRB(0.0, (topPadding - 125) / 2, 0.0, 0.0),
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  alignment: AlignmentDirectional.topCenter,
                  image: new AssetImage(
                    getImage(ImageList.LoginIconLogoForm),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Form loginForm() {
    return new Form(
        key: _formKey,
        child: new Padding(
          padding: EdgeInsets.fromLTRB(10.0, 40.0, 10.0, 10.0),
          child: new Column(
            children: <Widget>[
              new SizedBox(height: textFieldHeight, child: usernameTextField()),
              spacer(),
              new SizedBox(height: textFieldHeight, child: passwordTextField()),
              spacer(),
              new SizedBox(
                  height: textFieldHeight / 2, child: forgotPassButton()),
              spacer(),
              new SizedBox(height: textFieldHeight, child: loginButton()),
              spacer(),
              new SizedBox(height: textFieldHeight, child: registerButton())
            ],
          ),
        ));
  }

  SizedBox spacer() {
    return new SizedBox(
      height: 5.0,
    );
  }

  TextFormField usernameTextField() {
    return new TextFormField(
      controller: usernameController,
      style: new TextStyle(
        color: getColor(ColorList.DarkGreen, 1.0),
      ),
      autocorrect: false,
      decoration: new InputDecoration(
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(textFieldHeight / 2),
            borderSide: BorderSide(
              color: getColor(ColorList.BaseGray, 1.0),
            )),
        hintText: 'Enter your username',
        prefixIcon: const Icon(Icons.account_circle,
            color: Color.fromRGBO(106, 131, 71, 1.0)),
        hintStyle: TextStyle(
          color: Color.fromRGBO(106, 131, 71, 1.0),
        ),
        labelText: 'Username',
        labelStyle: TextStyle(color: Color.fromRGBO(106, 131, 71, 1.0)),
        border: new OutlineInputBorder(
            borderRadius: BorderRadius.circular(textFieldHeight / 2),
            borderSide: BorderSide(color: Color.fromRGBO(112, 112, 112, 1.0))),
        filled: true,
        fillColor: Color.fromRGBO(254, 252, 215, 1.0),
      ),
    );
  }

  TextFormField passwordTextField() {
    return new TextFormField(
      controller: passwordController,
      obscureText: true,
      style: new TextStyle(
        color: Color.fromRGBO(106, 131, 71, 1.0),
      ),
      autocorrect: false,
      decoration: new InputDecoration(
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(textFieldHeight / 2),
            borderSide: BorderSide(
              color: Color.fromRGBO(112, 112, 112, 1.0),
            )),
        hintText: 'Enter your password',
        prefixIcon: const Icon(
          Icons.lock,
          color: Color.fromRGBO(106, 131, 71, 1.0),
        ),
        hintStyle: TextStyle(
            color: Color.fromRGBO(106, 131, 71, 1.0),
            decorationStyle: TextDecorationStyle.dashed),
        labelText: 'Password',
        labelStyle:
            TextStyle(color: Color.fromRGBO(106, 131, 71, 1.0), height: 1.30),
        border: new OutlineInputBorder(
            borderRadius: BorderRadius.circular(textFieldHeight / 2),
            borderSide: BorderSide(color: Color.fromRGBO(112, 112, 112, 1.0))),
        filled: true,
        fillColor: Color.fromRGBO(254, 252, 215, 1.0),
      ),
    );
  }

  OutlineButton forgotPassButton() {
    return new OutlineButton(
      borderSide: new BorderSide(color: Colors.transparent),
      onPressed: () {},
      shape: new RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: new Text(
        'Forgot your password?',
        style: TextStyle(
          fontSize: 15.0,
          color: Color.fromRGBO(254, 252, 215, 1.0),
        ),
      ),
    );
  }

  RaisedButton loginButton() {
    return new RaisedButton(
      shape: new RoundedRectangleBorder(
          side:
              BorderSide(width: 1.0, color: Color.fromRGBO(254, 252, 215, 1.0)),
          borderRadius: BorderRadius.circular(textFieldHeight / 2)),
      color: Color.fromRGBO(106, 131, 71, 1.0),
      onPressed: () {
        trySubmit();
      },
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          new Container(
            child: new Text('Login',
                style: TextStyle(
                    color: Color.fromRGBO(254, 252, 215, 1.0), fontSize: 20.0)),
          )
        ],
      ),
    );
  }

  RaisedButton registerButton() {
    return new RaisedButton(
      shape: new RoundedRectangleBorder(
          side:
              BorderSide(width: 1.0, color: Color.fromRGBO(254, 252, 215, 1.0)),
          borderRadius: BorderRadius.circular(textFieldHeight / 2)),
      color: Color.fromRGBO(106, 131, 71, 1.0),
      onPressed: () {
        Navigator.pushNamed(context, '/register');
      },
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          new Container(
            child: new Text(
              'Register',
              style: TextStyle(
                  color: Color.fromRGBO(254, 252, 215, 1.0), fontSize: 20.0),
            ),
          )
        ],
      ),
    );
  }

  Future<Null> initConnectivity() async {
    String connectionStatus;
    connectionStatus = (await _connectivity.checkConnectivity()).toString();
    setState(() {
      _connectionStatus = connectionStatus;
    });
  }
}
