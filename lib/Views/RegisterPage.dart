import 'package:ayuda/Utilities/BlocProvider.dart';
import 'package:ayuda/Utilities/ImageProvider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:async';
import 'package:ayuda/Utilities/ColorProvider.dart';
import 'package:intl/intl.dart';
import 'package:ayuda/Blocs/UserRegisterBloc.dart';
import 'package:ayuda/Models/User.dart';

final usernameController = TextEditingController();
final emailController = TextEditingController();
final passwordController = TextEditingController();
final contactNumberController = TextEditingController();
final firstNameController = TextEditingController();
final lastNameController = TextEditingController();

UserRegisterBloc _register = new UserRegisterBloc();
List<DropdownMenuItem<String>> _genderDropdownItems;
String _genderSelected;

class RegisterPage extends StatefulWidget implements BlocBase {
  @override
  _RegisterUser createState() => _RegisterUser();

  @override
  void dispose() {
    usernameController.dispose();
    emailController.dispose();
    passwordController.dispose();
    contactNumberController.dispose();
    firstNameController.dispose();
    lastNameController.dispose();
  }
}

class _RegisterUser extends State<RegisterPage> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  String get username => usernameController.text;
  String get email => emailController.text;
  String get password => passwordController.text;
  String get contactNumber => contactNumberController.text;
  String get firstName => firstNameController.text;
  String get lastName => lastNameController.text;

  DateTime _date = new DateTime.now();
  DateFormat _birthDateFormat = new DateFormat('MMM dd, yyyy');
  Future<Null> _selectBirthDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: _date,
        firstDate: new DateTime(1960),
        lastDate: new DateTime(2020));

    if (picked != null && picked != _date) {
      print('Date selected: ${_date.toString()}');
      setState(() {
        _date = picked;
      });
    }
  }

  final double textFieldHeight = 60.0;
  final double topPadding = 125.0;

  void submitRegister() {
    _register.performRegister(new UserRegister(
        username,
        email,
        password,
        contactNumber,
        firstName,
        lastName,
        _genderSelected,
        _date.toString())).then((bool isSuccess) {
          if(isSuccess) {
            Navigator.popAndPushNamed(context, '/dashboard');
          }
        });
  }

  @override
  void initState() {
    _genderDropdownItems = _getGenderDropdownItems();
    _genderSelected = _genderDropdownItems[0].value;
    super.initState();
  }

  void changedGenderDropdownItem(String selectedItem) {
    setState(() {
      _genderSelected = selectedItem;
    });
  }

  List<DropdownMenuItem<String>> _getGenderDropdownItems() {
    List<DropdownMenuItem<String>> items = new List();

    items.add(new DropdownMenuItem(
      value: 'Male',
      child: new Text('Male'),
    ));

    items.add(new DropdownMenuItem(
      value: 'Female',
      child: new Text('Female'),
    ));

    return items;
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return new WillPopScope(
      onWillPop: () {
        SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
        Navigator.pop(context, false);
        return Future.value(false);
      },
      child: new Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: getColor(ColorList.WhiteCream, 1.0)
          ),
          backgroundColor: getColor(ColorList.DarkGreen, 1.0),
          title: new Text(
            'Register',
            style: TextStyle(
              color: getColor(ColorList.WhiteCream, 1.0),
            ),
          ),
          actions: <Widget>[registerButton()],
        ),
        body: new Container(
          height: double.infinity,
          width: double.infinity,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(getImage(ImageList.RegisterBackground)),
                  fit: BoxFit.cover)),
          child: new Padding(
            padding: EdgeInsets.all(20.0),
            child: new Container(
                padding: EdgeInsets.all(10.0),
                decoration: new BoxDecoration(
                    borderRadius: BorderRadius.circular(20.0),
                    color: getColor(ColorList.CreamGreen, 1.0)),
                child: new Container(
                  alignment: Alignment.center,
                  child: registerForm(),
                )),
          ),
        ),
      ),
    );
  }

  Form registerForm() {
    return new Form(
      key: _formKey,
      child: new ListView(
        shrinkWrap: true,
        children: <Widget>[
          new Padding(
            padding: EdgeInsets.only(bottom: 5.0),
            child: usernameTextField(),
          ),
          new Padding(
            padding: EdgeInsets.only(bottom: 5.0),
            child: emailTextField(),
          ),
          new Padding(
            padding: EdgeInsets.only(bottom: 5.0),
            child: passwordTextField(),
          ),
          new Padding(
            padding: EdgeInsets.only(bottom: 5.0),
            child: contactNumberTextField(),
          ),
          new Padding(
            padding: EdgeInsets.only(bottom: 5.0),
            child: firstNameTextField(),
          ),
          new Padding(
            padding: EdgeInsets.only(bottom: 5.0),
            child: lastNameTextField(),
          ),
          new Padding(
            padding: EdgeInsets.only(bottom: 5.0),
            child: genderDropdownContainer(),
          ),
          new Padding(
            padding: EdgeInsets.only(bottom: 5.0),
            child: selectBirthDateButton(),
          ),
        ],
      ),
    );
  }

  TextFormField usernameTextField() {
    return new TextFormField(
      controller: usernameController,
      style: new TextStyle(
        color: getColor(ColorList.DarkGreen, 1.0),
      ),
      autocorrect: false,
      decoration: new InputDecoration(
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(textFieldHeight / 2),
            borderSide: BorderSide(
              color: getColor(ColorList.BaseGray, 1.0),
            )),
        hintText: 'Enter your username',
        prefixIcon: const Icon(Icons.account_circle,
            color: Color.fromRGBO(106, 131, 71, 1.0)),
        hintStyle: TextStyle(
          color: Color.fromRGBO(106, 131, 71, 1.0),
        ),
        labelText: 'Username',
        labelStyle: TextStyle(color: Color.fromRGBO(106, 131, 71, 1.0)),
        border: new OutlineInputBorder(
            borderRadius: BorderRadius.circular(textFieldHeight / 2),
            borderSide: BorderSide(color: Color.fromRGBO(112, 112, 112, 1.0))),
        filled: true,
        fillColor: Color.fromRGBO(254, 252, 215, 1.0),
      ),
    );
  }

  TextFormField emailTextField() {
    return new TextFormField(
      controller: emailController,
      style: new TextStyle(
        color: getColor(ColorList.DarkGreen, 1.0),
      ),
      autocorrect: false,
      decoration: new InputDecoration(
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(textFieldHeight / 2),
            borderSide: BorderSide(
              color: getColor(ColorList.BaseGray, 1.0),
            )),
        hintText: 'Enter your email address',
        prefixIcon:
            const Icon(Icons.email, color: Color.fromRGBO(106, 131, 71, 1.0)),
        hintStyle: TextStyle(
          color: Color.fromRGBO(106, 131, 71, 1.0),
        ),
        labelText: 'Email Address',
        labelStyle: TextStyle(color: Color.fromRGBO(106, 131, 71, 1.0)),
        border: new OutlineInputBorder(
            borderRadius: BorderRadius.circular(textFieldHeight / 2),
            borderSide: BorderSide(color: Color.fromRGBO(112, 112, 112, 1.0))),
        filled: true,
        fillColor: Color.fromRGBO(254, 252, 215, 1.0),
      ),
    );
  }

  TextFormField passwordTextField() {
    return new TextFormField(
      controller: passwordController,
      obscureText: true,
      style: new TextStyle(
        color: getColor(ColorList.DarkGreen, 1.0),
      ),
      autocorrect: false,
      decoration: new InputDecoration(
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(textFieldHeight / 2),
            borderSide: BorderSide(
              color: getColor(ColorList.BaseGray, 1.0),
            )),
        hintText: 'Enter your password',
        prefixIcon:
            const Icon(Icons.lock, color: Color.fromRGBO(106, 131, 71, 1.0)),
        hintStyle: TextStyle(
          color: Color.fromRGBO(106, 131, 71, 1.0),
        ),
        labelText: 'Password',
        labelStyle: TextStyle(color: Color.fromRGBO(106, 131, 71, 1.0)),
        border: new OutlineInputBorder(
            borderRadius: BorderRadius.circular(textFieldHeight / 2),
            borderSide: BorderSide(color: Color.fromRGBO(112, 112, 112, 1.0))),
        filled: true,
        fillColor: Color.fromRGBO(254, 252, 215, 1.0),
      ),
    );
  }

  TextFormField contactNumberTextField() {
    return new TextFormField(
      controller: contactNumberController,
      style: new TextStyle(
        color: getColor(ColorList.DarkGreen, 1.0),
      ),
      autocorrect: false,
      decoration: new InputDecoration(
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(textFieldHeight / 2),
            borderSide: BorderSide(
              color: getColor(ColorList.BaseGray, 1.0),
            )),
        hintText: 'Enter your phone number',
        prefixIcon:
            const Icon(Icons.phone, color: Color.fromRGBO(106, 131, 71, 1.0)),
        hintStyle: TextStyle(
          color: Color.fromRGBO(106, 131, 71, 1.0),
        ),
        labelText: 'Phone number',
        labelStyle: TextStyle(color: Color.fromRGBO(106, 131, 71, 1.0)),
        border: new OutlineInputBorder(
            borderRadius: BorderRadius.circular(textFieldHeight / 2),
            borderSide: BorderSide(color: Color.fromRGBO(112, 112, 112, 1.0))),
        filled: true,
        fillColor: Color.fromRGBO(254, 252, 215, 1.0),
      ),
    );
  }

  TextFormField firstNameTextField() {
    return new TextFormField(
      controller: firstNameController,
      style: new TextStyle(
        color: getColor(ColorList.DarkGreen, 1.0),
      ),
      autocorrect: false,
      decoration: new InputDecoration(
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(textFieldHeight / 2),
            borderSide: BorderSide(
              color: getColor(ColorList.BaseGray, 1.0),
            )),
        hintText: 'Enter your first name',
        prefixIcon:
            const Icon(Icons.person, color: Color.fromRGBO(106, 131, 71, 1.0)),
        hintStyle: TextStyle(
          color: Color.fromRGBO(106, 131, 71, 1.0),
        ),
        labelText: 'First Name',
        labelStyle: TextStyle(color: Color.fromRGBO(106, 131, 71, 1.0)),
        border: new OutlineInputBorder(
            borderRadius: BorderRadius.circular(textFieldHeight / 2),
            borderSide: BorderSide(color: Color.fromRGBO(112, 112, 112, 1.0))),
        filled: true,
        fillColor: Color.fromRGBO(254, 252, 215, 1.0),
      ),
    );
  }

  TextFormField lastNameTextField() {
    return new TextFormField(
      controller: lastNameController,
      style: new TextStyle(
        color: getColor(ColorList.DarkGreen, 1.0),
      ),
      autocorrect: false,
      decoration: new InputDecoration(
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(textFieldHeight / 2),
            borderSide: BorderSide(
              color: getColor(ColorList.BaseGray, 1.0),
            )),
        hintText: 'Enter your last name',
        prefixIcon:
            const Icon(Icons.person, color: Color.fromRGBO(106, 131, 71, 1.0)),
        hintStyle: TextStyle(
          color: Color.fromRGBO(106, 131, 71, 1.0),
        ),
        labelText: 'Last Name',
        labelStyle: TextStyle(color: Color.fromRGBO(106, 131, 71, 1.0)),
        border: new OutlineInputBorder(
            borderRadius: BorderRadius.circular(textFieldHeight / 2),
            borderSide: BorderSide(color: Color.fromRGBO(112, 112, 112, 1.0))),
        filled: true,
        fillColor: Color.fromRGBO(254, 252, 215, 1.0),
      ),
    );
  }

  DropdownButton<String> selectGenderDropdown() {
    return new DropdownButton<String>(
      style:
          TextStyle(color: getColor(ColorList.DarkGreen, 1.0), fontSize: 17.0),
      value: _genderSelected,
      items: _genderDropdownItems,
      onChanged: changedGenderDropdownItem,
    );
  }

  Container genderDropdownContainer() {
    return new Container(
      padding: EdgeInsets.all(10.0),
      decoration: new BoxDecoration(
        borderRadius: BorderRadius.circular(textFieldHeight / 2),
        border: Border.all(
          width: 1.0,
          color: getColor(ColorList.BaseGray, 1.0),
        ),
        color: getColor(ColorList.WhiteCream, 1.0),
      ),
      child: new Stack(
        fit: StackFit.passthrough,
        children: <Widget>[
          new Positioned(
            left: 3.0,
            top: 7.0,
            child: new Icon(
              Icons.people,
              color: getColor(ColorList.DarkGreen, 1.0),
            ),
          ),
          new Padding(
            padding: EdgeInsets.only(left: 38.0),
            child: new SizedBox(
              height: textFieldHeight - 20,
              child: new DropdownButtonHideUnderline(
                child: selectGenderDropdown(),
              ),
            ),
          ),
        ],
      ),
    );
  }

  RaisedButton selectBirthDateButton() {
    return new RaisedButton(
      shape: new RoundedRectangleBorder(
        side: BorderSide(
          width: 1.0,
          color: getColor(ColorList.BaseGray, 1.0),
        ),
        borderRadius: BorderRadius.circular(textFieldHeight / 2),
      ),
      color: getColor(ColorList.WhiteCream, 1.0),
      onPressed: () {
        _selectBirthDate(context);
      },
      child: new Container(
        height: textFieldHeight - 5,
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            new Icon(
              Icons.calendar_today,
              color: getColor(ColorList.DarkGreen, 1.0),
            ),
            new Padding(
              padding: EdgeInsets.only(left: 10.0),
              child: new Text(
                '${_birthDateFormat.format(_date)}',
                style: new TextStyle(
                  color: getColor(ColorList.DarkGreen, 1.0),
                  fontSize: 17.0,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  IconButton registerButton() {
    return new IconButton(
      icon: Icon(Icons.save),
      onPressed: () {
        submitRegister();
      },
      iconSize: 35.0,
      color: getColor(ColorList.WhiteCream, 1.0),
      tooltip: 'Click here to register',
    );
  }
}
