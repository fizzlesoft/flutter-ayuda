import 'package:ayuda/Views/DashboardPage.dart';
import 'package:flutter/material.dart';
import 'package:ayuda/Views/LoginPage.dart';
import 'package:ayuda/Views/RegisterPage.dart';

final routes = {
  '/': (BuildContext context) => new LoginPage(),
  '/register': (BuildContext context) => new RegisterPage(),
  '/dashboard': (BuildContext context) => new DashboardPage()
};