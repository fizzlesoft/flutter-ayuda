class User {
  String _username;
  String _password;
  User(this._username, this._password);

  User.map(dynamic obj) {
    this._username = obj["username"];
    this._password = obj["password"];
  }

  String get username => _username;
  String get password => _password;

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["username"] = _username;
    map["password"] = _password;

    return map;
  }
}

class UserLoginCredential {
  final String token, loginKey;
  UserLoginCredential(this.token, this.loginKey);

  UserLoginCredential.fromJson(Map json)
      : token = json['token'],
        loginKey = json['loginKey'];
}

class UserRegister {
  final String username,
      emailAddress,
      password,
      contactNumber,
      firstName,
      lastName,
      gender,
      birthDate;

  UserRegister(
      this.username,
      this.emailAddress,
      this.password,
      this.contactNumber,
      this.firstName,
      this.lastName,
      this.gender,
      this.birthDate);

  UserRegister.fromJson(Map json)
      : username = json['username'],
        emailAddress = json['emailAddress'],
        password = json['password'],
        contactNumber = json['contactNumber'],
        firstName = json['firstName'],
        lastName = json['lastName'],
        gender = json['gender'],
        birthDate = json['birthDate'];
}

class BaseReponse {
  final bool response;
  BaseReponse(this.response);

  BaseReponse.fromJson(Map json) : response = json['isRegistered'];
}

class UserCredential {
  final int userID;
  final String username;
  final String emailAddress;
  final String contactNumber;
  final String userType;
  final String userKey;
  final String loginKey;
  final String birthDate;
  final int age;

  UserCredential(
    this.userID,
    this.username,
    this.emailAddress,
    this.contactNumber,
    this.userType,
    this.userKey,
    this.loginKey,
    this.birthDate,
    this.age
  );

  UserCredential.fromJson(Map json)
    : userID = json['userID'],
      username = json['username'],
      emailAddress = json['emailAddress'],
      contactNumber = json['contactNumber'],
      userType = json['userType'],
      userKey = json['userKey'],
      loginKey = json['loginKey'],
      birthDate = json['birthDate'],
      age = json['age'];
}
