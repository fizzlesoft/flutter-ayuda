import 'dart:async';
import 'dart:convert';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:http/http.dart' as http;
import 'package:ayuda/Models/User.dart';
import 'package:ayuda/Utilities/URLRequests.dart';
import 'package:ayuda/Utilities/NetworkUtils.dart';

class UserRegisterBloc {
  Future<bool> performRegister(UserRegister user) async {
    var fbUtil = new FBApi();
    bool result = false;
    if (user != null) {
      var body = jsonEncode({
        'username': user.username,
        'password': user.password,
        'emailAddress': user.emailAddress,
        'contactNumber': user.contactNumber,
        'firstName': user.firstName,
        'lastName': user.lastName,
        'gender': user.gender,
        'birthDate': user.birthDate
      });

      await http.post(
        URLRequest.URL_REGISTER,
        body: body,
        headers: {
          "Accept": "application/json",
          "content-type": "application/json"
        },
      ).then((http.Response response) {
        var isSuccess = BaseReponse.fromJson(jsonDecode(response.body));
        if(isSuccess.response) {
          fbUtil.signUpEmail(user.emailAddress, user.password)
          .then((FirebaseUser user) {
          });
        }
        result = true;
      });
    }
    return result;
  }
}
