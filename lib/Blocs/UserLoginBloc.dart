import 'dart:async';
import 'dart:convert';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:http/http.dart' as http;

import 'package:ayuda/Utilities/URLRequests.dart';
import 'package:ayuda/Utilities/NetworkUtils.dart';
import 'package:ayuda/Models/User.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
FBApi api = new FBApi();
class UserLoginBloc {

  Future<bool> doLogin(String username, String password) async {
    bool result = false;
    var body = jsonEncode({'username': username, 'password': password});    
    final SharedPreferences prefs = await _prefs;

    return await http.post(URLRequest.URL_LOGIN, body: body, headers: {
      "Accept": "application/json",
      "content-type": "application/json"
    }).then((http.Response res) {
      if (res.body != '') {
        var user = UserLoginCredential.fromJson(jsonDecode(res.body));
        if(user.token != null && user.loginKey != null) {
          prefs.setString('token', user.token);
          prefs.setString('loginKey', user.loginKey);
          getUserCredentials().then((UserCredential credentials) {
            api.signInEmail(credentials.emailAddress, password).then((FirebaseUser fbUser) {
              if(fbUser.uid != null) {
                return true;
              }
            });
          });
        }
        result = true;
      }
      return result;
    });
  }

  Future<UserCredential> getUserCredentials() async {
    final SharedPreferences prefs = await _prefs;
    return await http.get(URLRequest.URL_GET_USER_CREDENTIAL, headers: {
        "Accept": "application/json",
        "content-type": "application/json",
        "Authorization": "Bearer ${prefs.getString('token')}"
    }).then((http.Response res) {
      if(res.body != null) {
        Map response = json.decode(res.body);
        var credentials = UserCredential.fromJson(jsonDecode(json.encode(response['response'])));
        return credentials;
      }
    });
  }
}
